INSERT INTO client(id, firstname, lastname, discount) VALUES
    (201, 'Jan', 'Kowalski', 0.10),
    (202, 'Krzysztof', 'Nowak', 0.00),
    (203, 'Marcin', 'Bober', 0.05);

INSERT INTO dish(id, name, description, price, is_available) VALUES
    (301, 'Small kebab', 'Small kebab with chicken', 12.99, true),
    (302, 'Medium kebab', 'Medium kebab with chicken', 14.99, false),
    (303, 'Big kebab', 'Big kebab with chicken', 12.99, true),
    (304, 'XXL kebab', 'XXL kebab with chicken', 12.99, false);

INSERT INTO drink(id, name, description, price, portion, is_available) VALUES
    (401, 'Small water', 'Water in a bottle 0.5l', 2.99, 500, true),
    (402, 'Medium water', 'Water in a bottle 0.7l', 3.99, 700, false),
    (403, 'Big water', 'Water in a bottle 1.0l', 4.99, 1000, false),
    (404, 'XXL water', 'Water in a bottle 1.5l', 5.99, 1500, true);

INSERT INTO employee(id, firstname, lastname, pesel, position, salary, id_boss) VALUES
    (501, 'Janusz', 'Januszewski', '12345', 'RESTAURANT_MANAGER', 5500.00, 501),
    (502, 'Jan', 'Januszewski', '1234', 'KITCHEN_CHEF', 4500.00, 502),
    (503, 'Karolina', 'Karolinowska', '123457', 'BARMAN', 3500.00, 501),
    (504, 'Magdalena', 'Magdalenowska', '1234533', 'COOK', 2500.00, 501),
    (505, 'Piotr', 'Piotrowski', '123452323', 'KITCHEN_HELP', 1500.00, 502),
    (506, 'Adam', 'Adamowicz', '12333245', 'WAITER', 2000.00, 502);

INSERT INTO storage(id, name, quantity) VALUES
    (601, 'Small kebab', 10.00),
    (602, 'Medium kebab', 5.00),
    (603, 'Big kebab', 7.00),
    (604, 'XXL kebab', 0.00),
    (605, 'Small water', 11.00),
    (606, 'Medium water', 12.00),
    (607, 'Big water', 15.00),
    (608, 'XXL water', 7.00);

INSERT INTO bill (id, summary_price, tip, id_employee, id_client, date) VALUES
    (101, 9.99, 5.00, 501, 201, '2020-05-05T12:12:12'),
    (102, 19.99, 15.00, 502, 202, '2020-05-02T15:15:15'),
    (103, 29.99, 25.00, 503, 203, '2020-02-05T18:18:18'),
    (104, 39.99, 35.00, 504, 202, '2020-01-01T20:20:20');

SELECT setval ('public.hibernate_sequence', 11 , true );