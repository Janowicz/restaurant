-- DROP TABLE IF EXISTS bill, client, dish, drink, employee, storage, bill_dish, bill_drink, storage_dish, storage_drink;

CREATE TABLE dish(
    id serial NOT NULL,
    name character varying(100) NOT NULL,
    description character varying(250) NOT NULL,
    price double precision NOT NULL,
    is_available boolean NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE drink(
    id serial NOT NULL,
    name character varying(100) NOT NULL,
    description character varying(250) NOT NULL,
    price double precision NOT NULL,
    portion integer NOT NULL,
    is_available boolean NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE client(
    id serial NOT NULL,
    firstname character varying(50) NOT NULL,
    lastname character varying(50) NOT NULL,
    discount double precision NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE employee(
    id serial NOT NULL,
    firstname character varying(50) NOT NULL,
    lastname character varying(50) NOT NULL,
    pesel character varying(50) NOT NULL,
    position character varying(50) NOT NULL,
    salary double precision NOT NULL,
    boss_id integer NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (boss_id) REFERENCES employee(id)
);
CREATE TABLE bill(
    id serial NOT NULL,
    summary_price double precision NOT NULL,
    tip double precision NOT NULL,
    id_employee int NOT NULL,
    id_client int NOT NULL,
    date timestamp NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_employee) REFERENCES employee(id),
    FOREIGN KEY (id_client) REFERENCES client(id)
);
CREATE TABLE storage(
    id serial NOT NULL,
    name character varying(100) NOT NULL,
    quantity double precision NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE bill_dish(
    id_bill bigint NOT NULL,
    id_dish bigint NOT NULL,
    FOREIGN KEY (id_bill) REFERENCES bill (id),
    FOREIGN KEY (id_dish) REFERENCES dish (id)
);
CREATE TABLE bill_drink(
    id_bill bigint NOT NULL,
    id_drink bigint NOT NULL,
    FOREIGN KEY (id_bill) REFERENCES bill (id),
    FOREIGN KEY (id_drink) REFERENCES drink (id)
);
CREATE TABLE storage_dish(
    id_dish bigint NOT NULL,
    id_storage bigint NOT NULL,
    FOREIGN KEY (id_dish) REFERENCES dish (id),
    FOREIGN KEY (id_storage) REFERENCES storage (id)
);
CREATE TABLE storage_drink(
    id_drink bigint NOT NULL,
    id_storage bigint NOT NULL,
    FOREIGN KEY (id_drink) REFERENCES drink (id),
    FOREIGN KEY (id_storage) REFERENCES storage (id)
);