package com.example.restaurant.infrastructure.entity;

public enum EPosition {

    BARMAN,
    COOK,
    DEPUTY_KITCHEN_CHEF,
    KITCHEN_CHEF,
    KITCHEN_HELP,
    RESTAURANT_MANAGER,
    WAITER,
    WASHER,
    YOUNGER_COOK
}
