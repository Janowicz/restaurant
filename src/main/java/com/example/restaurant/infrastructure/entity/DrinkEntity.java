package com.example.restaurant.infrastructure.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "drink")
public class DrinkEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String description;

    private BigDecimal price;

    private Integer portion;

    private boolean isAvailable;

    @ManyToMany(mappedBy = "drinks", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<BillEntity> bills;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "storage_drink",
            joinColumns = @JoinColumn(name = "id_drink"),
            inverseJoinColumns = @JoinColumn(name = "id_storage"))
    private List<StorageEntity> storage;

    public DrinkEntity() {
    }

    public DrinkEntity(Long id,
                       String name,
                       String description,
                       BigDecimal price,
                       Integer portion,
                       boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.portion = portion;
        this.isAvailable = isAvailable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getPortion() {
        return portion;
    }

    public void setPortion(Integer portion) {
        this.portion = portion;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public List<BillEntity> getBills() {
        if (bills == null){
            bills = new ArrayList<>();
        }
        return bills;
    }

    public List<StorageEntity> getStorage() {
        return storage;
    }

    public void setStorage(List<StorageEntity> storage) {
        this.storage = storage;
    }

    public void setBills(List<BillEntity> bills) {
        this.bills = bills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrinkEntity that = (DrinkEntity) o;
        return isAvailable == that.isAvailable &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(price, that.price) &&
                Objects.equals(portion, that.portion) &&
                Objects.equals(bills, that.bills) &&
                Objects.equals(storage, that.storage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, portion, isAvailable, bills, storage);
    }

    @Override
    public String toString() {
        return "DrinkEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", portion=" + portion +
                ", isAvailable=" + isAvailable +
                ", bills=" + bills +
                ", storage=" + storage +
                '}';
    }
}
