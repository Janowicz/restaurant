package com.example.restaurant.infrastructure.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "bill")
public class BillEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private BigDecimal summaryPrice;
    private BigDecimal tip;

    @ManyToOne
    @JoinColumn(name = "id_employee")
    private EmployeeEntity employee;

    @ManyToOne
    @JoinColumn(name = "id_client")
    private ClientEntity client;

    private LocalDateTime date;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "bill_dish",
            joinColumns = @JoinColumn(name = "id_bill"),
            inverseJoinColumns = @JoinColumn(name = "id_dish"))
    private List<DishEntity> dishes;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "bill_drink",
            joinColumns = @JoinColumn(name = "id_bill"),
            inverseJoinColumns = @JoinColumn(name = "id_drink"))
    private List<DrinkEntity> drinks;

    public BillEntity() {
    }

    public BillEntity(Long id,
                      BigDecimal summaryPrice,
                      BigDecimal tip,
                      EmployeeEntity employeeEntity,
                      ClientEntity clientEntity,
                      LocalDateTime date) {
        this.id = id;
        this.summaryPrice = summaryPrice;
        this.tip = tip;
        this.employee = employeeEntity;
        this.client = clientEntity;
        this.date = date;
    }

    public BillEntity(Long id,
                      BigDecimal summaryPrice,
                      BigDecimal tip,
                      EmployeeEntity employee,
                      ClientEntity client,
                      LocalDateTime date,
                      List<DishEntity> dishes,
                      List<DrinkEntity> drinks) {
        this.id = id;
        this.summaryPrice = summaryPrice;
        this.tip = tip;
        this.employee = employee;
        this.client = client;
        this.date = date;
        this.dishes = dishes;
        this.drinks = drinks;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSummaryPrice() {
        return summaryPrice;
    }

    public void setSummaryPrice(BigDecimal summaryPrice) {
        this.summaryPrice = summaryPrice;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public void setTip(BigDecimal tip) {
        this.tip = tip;
    }

    public EmployeeEntity getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeEntity employeeEntity) {
        this.employee = employeeEntity;
    }

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity clientEntity) {
        this.client = clientEntity;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public List<DishEntity> getDishes() {
        if (dishes == null){
            dishes = new ArrayList<>();
        }
        return dishes;
    }

    public void setDishes(List<DishEntity> dishes) {
        this.dishes = dishes;
    }

    public List<DrinkEntity> getDrinks() {
        if (drinks == null){
            drinks = new ArrayList<>();
        }
        return drinks;
    }

    public void setDrinks(List<DrinkEntity> drinks) {
        this.drinks = drinks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BillEntity that = (BillEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(summaryPrice, that.summaryPrice) &&
                Objects.equals(tip, that.tip) &&
                Objects.equals(employee, that.employee) &&
                Objects.equals(client, that.client) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, summaryPrice, tip, employee, client, date);
    }

    @Override
    public String toString() {
        return "BillEntity{" +
                "id=" + id +
                ", summaryPrice=" + summaryPrice +
                ", tip=" + tip +
                ", employeeEntity=" + employee +
                ", clientEntity=" + client +
                ", billDate=" + date +
                '}';
    }
}
