package com.example.restaurant.infrastructure.entity;

import com.example.restaurant.domain.model.Employee;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "employee")
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String firstname;

    private String lastname;

    private String pesel;

    @Enumerated(EnumType.STRING)
    private EPosition position;

    private BigDecimal salary;

    @ManyToOne
    @JoinColumn(name = "id_boss")
    private EmployeeEntity boss;

    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BillEntity> bills;

    public EmployeeEntity() {
    }

    public EmployeeEntity(Long id,
                          String firstname,
                          String lastname,
                          String pesel,
                          EPosition position,
                          BigDecimal salary,
                          EmployeeEntity boss) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.pesel = pesel;
        this.position = position;
        this.salary = salary;
        this.boss = boss;
    }

    public EmployeeEntity(Long id,
                          String firstname,
                          String lastname,
                          String pesel,
                          EPosition position,
                          BigDecimal salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.pesel = pesel;
        this.position = position;
        this.salary = salary;
    }

    public EmployeeEntity(Employee employee) {
        this.id = employee.getId();
        this.firstname = employee.getFirstname();
        this.lastname = employee.getLastname();
        this.pesel = employee.getPesel();
        this.position = employee.getPosition();
        this.salary = employee.getSalary();
        this.boss = new EmployeeEntity(
                employee.getBoss().getId(),
                employee.getBoss().getFirstname(),
                employee.getBoss().getLastname(),
                employee.getBoss().getPesel(),
                employee.getBoss().getPosition(),
                employee.getBoss().getSalary());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public EPosition getPosition() {
        return position;
    }

    public void setPosition(EPosition position) {
        this.position = position;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public EmployeeEntity getBoss() {
        return boss;
    }

    public void setBoss(EmployeeEntity boss) {
        this.boss = boss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeEntity that = (EmployeeEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(firstname, that.firstname) &&
                Objects.equals(lastname, that.lastname) &&
                Objects.equals(pesel, that.pesel) &&
                position == that.position &&
                Objects.equals(salary, that.salary) &&
                Objects.equals(boss, that.boss);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, lastname, pesel, position, salary, boss);
    }

    @Override
    public String toString() {
        return "EmployeeEntity{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", position=" + position +
                ", salary=" + salary +
                ", boss=" + boss +
                '}';
    }
}
