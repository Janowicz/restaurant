package com.example.restaurant.infrastructure.adapter;

import com.example.restaurant.domain.model.*;
import com.example.restaurant.domain.repository.BillRepository;
import com.example.restaurant.exception.NotFoundException;
import com.example.restaurant.infrastructure.entity.*;
import com.example.restaurant.infrastructure.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class BillRepositoryImpl implements BillRepository {

    private final BillEntityRepository billEntityRepository;

    private final ClientRepositoryImpl clientRepository;
    private final EmployeeRepositoryImpl employeeRepository;
    private final DishRepositoryImpl dishRepository;
    private final DrinkRepositoryImpl drinkRepository;

    @Autowired
    public BillRepositoryImpl(BillEntityRepository billEntityRepository,
                              ClientRepositoryImpl clientRepository,
                              EmployeeRepositoryImpl employeeRepository,
                              DishRepositoryImpl dishRepository,
                              DrinkRepositoryImpl drinkRepository){
        this.billEntityRepository = billEntityRepository;
        this.clientRepository = clientRepository;
        this.employeeRepository = employeeRepository;
        this.dishRepository = dishRepository;

        this.drinkRepository = drinkRepository;
    }

    public Bill toDomain(BillEntity billEntity){
        return new Bill(
                billEntity.getId(),
                billEntity.getSummaryPrice(),
                billEntity.getTip(),
                employeeRepository.toDomain(billEntity.getEmployee()),
                clientRepository.toDomain(billEntity.getClient()),
                billEntity.getDate(),
                billEntity.getDishes().stream().map(dishRepository::toDomain).collect(Collectors.toList()),
                billEntity.getDrinks().stream().map(drinkRepository::toDomain).collect(Collectors.toList()));
    }

    @Override
    public Bill createBill(BigDecimal summaryPrice, BigDecimal tip, Employee employee, Client client, LocalDateTime date) {

        BillEntity billEntity = new BillEntity(
                null,
                summaryPrice,
                tip,
                new EmployeeEntity(employee),
                new ClientEntity(client),
                date,
                new ArrayList<DishEntity>(),
                new ArrayList<DrinkEntity>());
        billEntityRepository.save(billEntity);

        return toDomain(billEntity);
    }

    @Override
    @Transactional
    public Optional<Bill> updateBill(Long id, BigDecimal summaryPrice, BigDecimal tip, Employee employee, Client client, LocalDateTime date) {

        Optional<BillEntity> optionalBillEntity = billEntityRepository.findById(id);
        if (!optionalBillEntity.isPresent()){
            throw new NotFoundException();
        }

        BillEntity updateBill = optionalBillEntity.get();
        updateBill.setSummaryPrice(summaryPrice);
        updateBill.setTip(tip);
        updateBill.setEmployee(employeeRepository.toHibernate(employee));
        updateBill.setClient(clientRepository.toHibernate(client));
        updateBill.setDate(date);

        return optionalBillEntity.map(this::toDomain);
    }

    @Override
    public Optional<Bill> getBillById(Long id) {
        return billEntityRepository.findById(id).map(this::toDomain);
    }

    @Override
    public Bill addDish(Dish dish, Bill bill) {

        BillEntity billEntity = billEntityRepository.findById(bill.getId()).get();
        billEntity.getDishes().add(dishRepository.toHibernate(dishRepository.getDishById(dish.getId()).get()));

        billEntityRepository.save(billEntity);

        return toDomain(billEntity);
    }

    @Override
    public Bill addDrink(Drink drink, Bill bill) {

        BillEntity billEntity = billEntityRepository.findById(bill.getId()).get();
        billEntity.getDrinks().add(drinkRepository.toHibernate(drinkRepository.getDrinkById(drink.getId()).get()));

        billEntityRepository.save(billEntity);

        return toDomain(billEntity);
    }

    @Override
    public Bill removeDish(Dish dish, Bill bill) {

        BillEntity billEntity = billEntityRepository.findById(bill.getId()).get();
        billEntity.getDishes().remove(dishRepository.toHibernate(dishRepository.getDishById(dish.getId()).get()));

        billEntityRepository.save(billEntity);

        return toDomain(billEntity);
    }

    @Override
    public Bill removeDrink(Drink drink, Bill bill) {

        BillEntity billEntity = billEntityRepository.findById(bill.getId()).get();
        billEntity.getDrinks().remove(drinkRepository.toHibernate(drinkRepository.getDrinkById(drink.getId()).get()));

        billEntityRepository.save(billEntity);

        return toDomain(billEntity);
    }

//    @Override
//    public Integer changeQuantityDish(Long dishId, Long billId, Integer quantity) {
//        return null;
//    }

//    @Override
//    public Integer changeQuantityDrink(Long drinkId, Long billId, Integer quantity) {
//        return null;
//    }

    @Override
    public List<Bill> getAllBills() {
        return billEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }

//    @Override
//    public List<Bill> getBillsByDate(LocalDate date) {
//        return billEntityRepository.findAllByDate(date).stream().map(this::toDomain).collect(Collectors.toList());
//    }

    @Override
    public List<Bill> getAllBillsByClientId(Long clientId) {
        return billEntityRepository.findAllByClient(clientId).stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public List<Bill> getAllBillsByEmployeeId(Long employeeId) {
        return billEntityRepository.findAllByEmployee(employeeId).stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeBill(Long id) {
        billEntityRepository.deleteById(id);
    }
}

