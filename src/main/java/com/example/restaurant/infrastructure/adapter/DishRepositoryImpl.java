package com.example.restaurant.infrastructure.adapter;

import com.example.restaurant.domain.model.Dish;
import com.example.restaurant.domain.repository.DishRepository;
import com.example.restaurant.exception.NotFoundException;
import com.example.restaurant.infrastructure.entity.DishEntity;
import com.example.restaurant.infrastructure.repository.DishEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class DishRepositoryImpl implements DishRepository {

    private final DishEntityRepository dishEntityRepository;

    @Autowired
    public DishRepositoryImpl(DishEntityRepository dishEntityRepository) {
        this.dishEntityRepository = dishEntityRepository;
    }

    public Dish toDomain(DishEntity dishEntity){
        return new Dish(
                dishEntity.getId(),
                dishEntity.getName(),
                dishEntity.getDescription(),
                dishEntity.getPrice(),
                dishEntity.isAvailable());
    }

    public DishEntity toHibernate(Dish dish){
        return new DishEntity(
                dish.getId(),
                dish.getName(),
                dish.getDescription(),
                dish.getPrice(),
                dish.isAvailable());
    }

    @Override
    public Dish createDish(String name, String description, BigDecimal price, Boolean isAvailable) {

        DishEntity dishEntity = new DishEntity(null, name, description, price, isAvailable);
        dishEntityRepository.save(dishEntity);

        return toDomain(dishEntity);
    }

    @Override
    public Optional<Dish> getDishById(Long id) {
        return dishEntityRepository.findById(id).map(this::toDomain);
    }

    @Override
    public Optional<Dish> getDishByName(String name) {
        return dishEntityRepository.findByName(name).map(this::toDomain);
    }

    @Override
    public List<Dish> getAllDishes() {
        return dishEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public Optional<Dish> updateDish(Long id, String name, String description, BigDecimal price, Boolean isAvailable) {

        Optional<DishEntity> optionalDishEntity = dishEntityRepository.findById(id);
        if (!optionalDishEntity.isPresent()){
            throw new NotFoundException();
        }

        DishEntity updateDish = optionalDishEntity.get();

        updateDish.setName(name);
        updateDish.setDescription(description);
        updateDish.setPrice(price);
        updateDish.setAvailable(isAvailable);

        dishEntityRepository.save(updateDish);

        return optionalDishEntity.map(this::toDomain);
    }

    @Override
    public void removeDish(Long id) {
        dishEntityRepository.deleteById(id);
    }
}
