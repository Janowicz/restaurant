package com.example.restaurant.infrastructure.adapter;

import com.example.restaurant.domain.model.Drink;
import com.example.restaurant.domain.repository.DrinkRepository;
import com.example.restaurant.exception.NotFoundException;
import com.example.restaurant.infrastructure.entity.DrinkEntity;
import com.example.restaurant.infrastructure.repository.DrinkEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class DrinkRepositoryImpl implements DrinkRepository {

    private final DrinkEntityRepository drinkEntityRepository;

    @Autowired
    public DrinkRepositoryImpl(DrinkEntityRepository drinkEntityRepository) {
        this.drinkEntityRepository = drinkEntityRepository;
    }

    public Drink toDomain(DrinkEntity drinkEntity){
        return new Drink(
                drinkEntity.getId(),
                drinkEntity.getName(),
                drinkEntity.getDescription(),
                drinkEntity.getPrice(),
                drinkEntity.getPortion(),
                drinkEntity.isAvailable());
    }

    public DrinkEntity toHibernate(Drink drink){
        return new DrinkEntity(
                drink.getId(),
                drink.getName(),
                drink.getDescription(),
                drink.getPrice(),
                drink.getPortion(),
                drink.isAvailable());
    }

    @Override
    public Drink createDrink(String name, String description, BigDecimal price, Integer portion, boolean isAvailable) {

        DrinkEntity drinkEntity = new DrinkEntity(null, name, description, price, portion, isAvailable);
        drinkEntityRepository.save(drinkEntity);

        return toDomain(drinkEntity);
    }

    @Override
    public Optional<Drink> getDrinkById(Long id) {
        return drinkEntityRepository.findById(id).map(this::toDomain);
    }

    @Override
    public Optional<Drink> getDrinkByName(String name) {
        return drinkEntityRepository.findByName(name).map(this::toDomain);
    }

    @Override
    public List<Drink> getAllDrinks() {
        return drinkEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public Optional<Drink> updateDrink(Long id, String name, String description, BigDecimal price, Integer portion, boolean isAvailable) {

        Optional<DrinkEntity> drinkEntity = drinkEntityRepository.findById(id);
        if (!drinkEntity.isPresent()){
            throw new NotFoundException();
        }

        DrinkEntity updateDrink = drinkEntity.get();
        updateDrink.setName(name);
        updateDrink.setDescription(description);
        updateDrink.setPrice(price);
        updateDrink.setPortion(portion);
        updateDrink.setAvailable(isAvailable);

        drinkEntityRepository.save(updateDrink);

        return drinkEntity.map(this::toDomain);
    }

    @Override
    public void removeDrink(Long id) {
        drinkEntityRepository.deleteById(id);
    }
}
