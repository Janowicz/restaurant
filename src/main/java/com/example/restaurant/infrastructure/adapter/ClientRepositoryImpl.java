package com.example.restaurant.infrastructure.adapter;

import com.example.restaurant.domain.model.Client;
import com.example.restaurant.domain.repository.ClientRepository;
import com.example.restaurant.exception.NotFoundException;
import com.example.restaurant.infrastructure.entity.ClientEntity;
import com.example.restaurant.infrastructure.repository.ClientEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ClientRepositoryImpl implements ClientRepository {

    private final ClientEntityRepository clientEntityRepository;

    @Autowired
    public ClientRepositoryImpl(ClientEntityRepository clientEntityRepository) {
        this.clientEntityRepository = clientEntityRepository;
    }

    public Client toDomain(ClientEntity clientEntity){
        return new Client(
                clientEntity.getId(),
                clientEntity.getFirstname(),
                clientEntity.getLastname(),
                clientEntity.getDiscount());
    }

    public ClientEntity toHibernate(Client client){
        return new ClientEntity(
                client.getId(),
                client.getFirstname(),
                client.getLastname(),
                client.getDiscount());
    }

    @Override
    public Client createClient(String firstname, String lastname, BigDecimal discount) {

        ClientEntity clientEntity = new ClientEntity(null, firstname, lastname, discount);
        clientEntityRepository.save(clientEntity);
        return toDomain(clientEntity);
    }

    @Override
    public Optional<Client> updateClient(Long id, String firstname, String lastname, BigDecimal discount) {

        Optional<ClientEntity> optionalClientEntity = clientEntityRepository.findById(id);
        if (!optionalClientEntity.isPresent()){
            throw new NotFoundException();
        }

        ClientEntity clientEntity = optionalClientEntity.get();

        clientEntity.setFirstname(firstname);
        clientEntity.setLastname(lastname);
        clientEntity.setDiscount(discount);

        clientEntityRepository.save(clientEntity);

        return optionalClientEntity.map(this::toDomain);
    }

    @Override
    public Optional<Client> getClientById(Long id) {
        return clientEntityRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Client> getAllClients() {
        return clientEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public List<Client> getAllClientsByFirstname(String firstname) {
        return clientEntityRepository.findAllByFirstname(firstname).stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public List<Client> getAllClientsByLastname(String lastname) {
        return clientEntityRepository.findAllByLastname(lastname).stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeClient(Long id) {
        clientEntityRepository.deleteById(id);
    }
}
