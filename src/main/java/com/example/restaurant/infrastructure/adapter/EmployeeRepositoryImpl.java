package com.example.restaurant.infrastructure.adapter;

import com.example.restaurant.domain.model.Employee;
import com.example.restaurant.domain.repository.EmployeeRepository;
import com.example.restaurant.exception.NotFoundException;
import com.example.restaurant.infrastructure.entity.EPosition;
import com.example.restaurant.infrastructure.entity.EmployeeEntity;
import com.example.restaurant.infrastructure.repository.EmployeeEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private final EmployeeEntityRepository employeeEntityRepository;

    @Autowired
    public EmployeeRepositoryImpl(EmployeeEntityRepository employeeEntityRepository) {
        this.employeeEntityRepository = employeeEntityRepository;
    }

    public Employee toDomain(EmployeeEntity employeeEntity){
        return new Employee(
                employeeEntity.getId(),
                employeeEntity.getFirstname(),
                employeeEntity.getLastname(),
                employeeEntity.getPesel(),
                employeeEntity.getPosition(),
                employeeEntity.getSalary());
    }

    public EmployeeEntity toHibernate(Employee employee){
        return new EmployeeEntity(
                employee.getId(),
                employee.getFirstname(),
                employee.getLastname(),
                employee.getPesel(),
                employee.getPosition(),
                employee.getSalary());
    }

    public Employee toDomainWithBoss(EmployeeEntity employeeEntity){
        return new Employee(
                employeeEntity.getId(),
                employeeEntity.getFirstname(),
                employeeEntity.getLastname(),
                employeeEntity.getPesel(),
                employeeEntity.getPosition(),
                employeeEntity.getSalary(),
                this.toDomain(employeeEntity.getBoss()));
    }

    @Override
    public Employee createEmployee(String firstname, String lastname, String pesel, EPosition position, BigDecimal salary, Employee boss) {

        EmployeeEntity bossEmployeeEntity = new EmployeeEntity(
                boss.getId(),
                boss.getFirstname(),
                boss.getLastname(),
                boss.getPesel(),
                boss.getPosition(),
                boss.getSalary()
        );

        EmployeeEntity employeeEntity = new EmployeeEntity(null, firstname, lastname, pesel, position, salary, bossEmployeeEntity);
        employeeEntityRepository.save(employeeEntity);

        return toDomain(employeeEntity);
    }

    @Override
    public Employee updateEmployee(Long id, String firstname, String lastname, String pesel,
                                   EPosition position, BigDecimal salary, Employee boss) {

        Optional<EmployeeEntity> optionalEmployeeEntity = employeeEntityRepository.findById(id);
        if (!optionalEmployeeEntity.isPresent()){
            throw new NotFoundException();
        }

        Optional<EmployeeEntity> optionalBossEntity = employeeEntityRepository.findById(boss.getId());
        if (!optionalBossEntity.isPresent()){
            throw new NotFoundException();
        }

        EmployeeEntity employeeToUpdate = optionalEmployeeEntity.get();

        employeeToUpdate.setFirstname(firstname);
        employeeToUpdate.setLastname(lastname);
        employeeToUpdate.setPesel(pesel);
        employeeToUpdate.setPosition(position);
        employeeToUpdate.setSalary(salary);
        employeeToUpdate.setBoss(optionalBossEntity.get());

        employeeEntityRepository.save(employeeToUpdate);

        return toDomain(employeeToUpdate);
    }

    @Override
    public Optional<Employee> getEmployeeById(Long id) {
        return employeeEntityRepository.findById(id).map(this::toDomainWithBoss);
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeEntityRepository.findAll().stream().map(this::toDomainWithBoss).collect(Collectors.toList());
    }

    @Override
    public void removeEmployee(Long id) {
        employeeEntityRepository.deleteById(id);
    }
}
