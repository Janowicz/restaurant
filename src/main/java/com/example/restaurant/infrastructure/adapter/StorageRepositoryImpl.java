package com.example.restaurant.infrastructure.adapter;

import com.example.restaurant.domain.model.Storage;
import com.example.restaurant.domain.repository.StorageRepository;
import com.example.restaurant.exception.NotFoundException;
import com.example.restaurant.infrastructure.entity.DishEntity;
import com.example.restaurant.infrastructure.entity.DrinkEntity;
import com.example.restaurant.infrastructure.entity.StorageEntity;
import com.example.restaurant.infrastructure.repository.StorageEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class StorageRepositoryImpl implements StorageRepository {

    private final StorageEntityRepository storageEntityRepository;

    @Autowired
    public StorageRepositoryImpl(StorageEntityRepository storageEntityRepository) {
        this.storageEntityRepository = storageEntityRepository;
    }

    public Storage toDomain(StorageEntity storageEntity){
        return new Storage(
                storageEntity.getId(),
                storageEntity.getName(),
                storageEntity.getQuantity());
    }

    @Override
    public Storage createStorage(String name, Double quantity) {

        StorageEntity storageEntity = new StorageEntity(null, name, quantity);
        storageEntityRepository.save(storageEntity);

        return toDomain(storageEntity);
    }

    @Override
    public Storage updateStorage(Long id, String name, Double quantity) {

        Optional<StorageEntity> optionalStorageEntity = storageEntityRepository.findById(id);
        if (!optionalStorageEntity.isPresent()){
            throw new NotFoundException();
        }

        StorageEntity storageEntity = optionalStorageEntity.get();

        storageEntity.setName(name);
        storageEntity.setQuantity(quantity);

        storageEntityRepository.save(storageEntity);

        return toDomain(storageEntity);
    }

    @Override
    public Optional<Storage> getStorageById(Long id) {
        return storageEntityRepository.findById(id).map(this::toDomain);
    }

    @Override
    public Optional<Storage> getStorageByName(String name) {
        return storageEntityRepository.findByName(name).map(this::toDomain);
    }

    @Override
    public List<Storage> getAllStorages() {
        return storageEntityRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void removeStorage(Long id) {
        storageEntityRepository.deleteById(id);
    }
}
