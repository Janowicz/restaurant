package com.example.restaurant.infrastructure.repository;

import com.example.restaurant.domain.model.Storage;
import com.example.restaurant.infrastructure.entity.DishEntity;
import com.example.restaurant.infrastructure.entity.DrinkEntity;
import com.example.restaurant.infrastructure.entity.StorageEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StorageEntityRepository extends CrudRepository<StorageEntity, Long> {

    Optional<StorageEntity> findByName(String name);

    List<StorageEntity> findAll();

//    Optional<StorageEntity> findByName(DishEntity dishEntity);

}
