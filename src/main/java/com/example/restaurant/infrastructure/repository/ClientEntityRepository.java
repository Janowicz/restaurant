package com.example.restaurant.infrastructure.repository;

import com.example.restaurant.infrastructure.entity.ClientEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ClientEntityRepository extends CrudRepository<ClientEntity, Long> {

    List<ClientEntity> findAllByFirstname (String firstname);

    List<ClientEntity> findAllByLastname (String lastname);

    List<ClientEntity> findAll();
}
