package com.example.restaurant.infrastructure.repository;

import com.example.restaurant.infrastructure.entity.EPosition;
import com.example.restaurant.infrastructure.entity.EmployeeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface EmployeeEntityRepository extends CrudRepository<EmployeeEntity, Long> {

    List<EmployeeEntity> findAllByPosition(EPosition position);

    List<EmployeeEntity> findAllByFirstname(String firstname);

    List<EmployeeEntity> findAllByLastname(String lastname);

    List<EmployeeEntity> findAllBySalary(BigDecimal salary);

    Optional<EmployeeEntity> findByPesel(String pesel);

    List<EmployeeEntity> findAll();

}
