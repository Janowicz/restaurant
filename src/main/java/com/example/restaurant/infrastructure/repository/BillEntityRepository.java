package com.example.restaurant.infrastructure.repository;

import com.example.restaurant.infrastructure.entity.BillEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public interface BillEntityRepository extends CrudRepository<BillEntity, Long>, JpaRepository<BillEntity, Long> {

    List<BillEntity> findAllByEmployee(Long idEmployee);

    List<BillEntity> findAllByClient(Long idClient);

//    @Query(value = "SELECT s FROM BillEntity s WHERE function('DATE_TRUNC','day',s.startTime)=?1")
//    List<BillEntity> findAllByDate(LocalDate date);
}
