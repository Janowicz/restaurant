package com.example.restaurant.infrastructure.repository;

import com.example.restaurant.domain.model.Dish;
import com.example.restaurant.infrastructure.entity.DishEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface DishEntityRepository extends CrudRepository<DishEntity, Long>{

    @Override
    Optional<DishEntity> findById(Long aLong);

    Optional<DishEntity> findByName(String name);

    List<DishEntity> findAll();

}
