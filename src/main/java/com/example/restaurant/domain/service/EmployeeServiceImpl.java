package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.Employee;
import com.example.restaurant.domain.repository.EmployeeRepository;
import com.example.restaurant.infrastructure.entity.EPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Long createEmployee(String firstname, String lastname, String pesel, EPosition position, BigDecimal salary, Employee boss) {

        Employee employee = employeeRepository.createEmployee(firstname, lastname, pesel, position, salary, boss);

        return employee.getId();
    }

    @Override
    public Long updateEmployee(Long id, String firstname, String lastname, String pesel, EPosition position, BigDecimal salary, Employee boss) {

        Employee employee = employeeRepository.updateEmployee(id, firstname, lastname, pesel, position, salary, boss);

        return employee.getId();
    }

    @Override
    public Optional<Employee> getEmployee(Long id) {

        return employeeRepository.getEmployeeById(id);
    }

    @Override
    public List<Employee> getAllEmployees() {

        return employeeRepository.getAllEmployees();
    }

    @Override
    public void removeEmployee(Long id) {

        employeeRepository.removeEmployee(id);
    }
}
