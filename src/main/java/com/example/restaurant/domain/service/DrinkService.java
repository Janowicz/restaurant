package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.Drink;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface DrinkService {

    Long createDrink(String name, String description, BigDecimal price, Integer portion, boolean isAvailable);

    Long updateDrink(Long id, String name, String description, BigDecimal price, Integer portion, boolean isAvailable);

    Optional<Drink> getDrinkById(Long id);

    Optional<Drink> getDrinkByName(String name);

    List<Drink> getAllDrinks();

    void removeDrink(Long id);
}
