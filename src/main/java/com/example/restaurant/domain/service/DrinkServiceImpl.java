package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.Dish;
import com.example.restaurant.domain.model.Drink;
import com.example.restaurant.domain.repository.DrinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class DrinkServiceImpl implements DrinkService {

    private final DrinkRepository drinkRepository;

    @Autowired
    public DrinkServiceImpl(DrinkRepository drinkRepository) {
        this.drinkRepository = drinkRepository;
    }

    @Override
    public Long createDrink(String name, String description, BigDecimal price, Integer portion, boolean isAvailable) {

        Drink drink = drinkRepository.createDrink(name, description, price, portion, isAvailable);

        return drink.getId();
    }

    @Override
    public Long updateDrink(Long id, String name, String description, BigDecimal price, Integer portion, boolean isAvailable) {

        Optional<Drink> dish = drinkRepository.updateDrink(id, name, description, price, portion, isAvailable);

        return dish.get().getId();
    }

    @Override
    public Optional<Drink> getDrinkById(Long id) {

        return drinkRepository.getDrinkById(id);
    }

    @Override
    public Optional<Drink> getDrinkByName(String name) {

        return drinkRepository.getDrinkByName(name);
    }

    @Override
    public List<Drink> getAllDrinks() {
        return drinkRepository.getAllDrinks();
    }

    @Override
    public void removeDrink(Long id) {
        drinkRepository.removeDrink(id);
    }
}
