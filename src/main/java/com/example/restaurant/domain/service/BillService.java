package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BillService {

    Long createBill(Long employeeId, Long clientId);

    Long updateBill(Long id, BigDecimal summaryPrice, BigDecimal tip, Employee employee, Client client, LocalDateTime date);

    Optional<Bill> getBillById(Long id);

    List<Bill> getAllBills();

//    List<Bill> getBillsByDate(LocalDate date);

    Bill addDrinkToBill(Long drinkId, Long billId);

    Bill addDishToBill(Long dishId, Long billId);

    void removeDish(Long dishId, Long billId);

    void removeDrink(Long drinkId, Long billId);

    void removeBill(Long id);
}
