package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.Storage;

import java.util.List;
import java.util.Optional;

public interface StorageService {

    Long createStorage(String name, Double quantity);

    Long updateStorage(Long id, String name, Double quantity);

    Optional<Storage> getStorageById(Long id);

    Optional<Storage> getStorageByName(String name);

    List<Storage> getAllStorages();

    void removeStorage(Long id);
}
