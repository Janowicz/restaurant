package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.Client;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface  ClientService {

    Long createClient(String firstname, String lastname, BigDecimal discount);

    Long updateClient(Long id, String firstname, String lastname, BigDecimal discount);

    Optional<Client> getClient(Long id);

    List<Client> getAllClients();

    void removeClient(Long id);
}
