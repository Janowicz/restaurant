package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.Dish;
import com.example.restaurant.domain.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class DishServiceImpl implements DishService {

    private final DishRepository dishRepository;

    @Autowired
    public DishServiceImpl(DishRepository dishRepository) {
        this.dishRepository = dishRepository;
    }

    @Override
    public Long createDish(String name, String description, BigDecimal price, Boolean isAvailable) {

        Dish dish = dishRepository.createDish(name, description, price, isAvailable);

        return dish.getId();
    }

    @Override
    public Long updateDish(Long id, String name, String description, BigDecimal price, Boolean isAvailable) {

        Optional<Dish> dish = dishRepository.updateDish(id, name, description, price, isAvailable);

        return dish.get().getId();
    }

    @Override
    public Optional<Dish> getDishById(Long id) {
        return dishRepository.getDishById(id);
    }

    @Override
    public Optional<Dish> getDishByName(String name) {
        return dishRepository.getDishByName(name);
    }

    @Override
    public List<Dish> getAllDishes() {
        return dishRepository.getAllDishes();
    }

    @Override
    public void removeDish(Long id) {
        dishRepository.removeDish(id);
    }
}
