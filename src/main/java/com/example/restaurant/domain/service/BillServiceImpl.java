package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.*;
import com.example.restaurant.domain.repository.*;
import com.example.restaurant.exception.NotAvailableException;
import com.example.restaurant.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class BillServiceImpl implements BillService{

    private final BillRepository billRepository;
    private final ClientRepository clientRepository;
    private final EmployeeRepository employeeRepository;
    private final DishRepository dishRepository;
    private final DrinkRepository drinkRepository;
    private final StorageRepository storageRepository;

    private static final BigDecimal BIG_DECIMAL_ZERO = new BigDecimal(0.0);

    @Autowired
    public BillServiceImpl(BillRepository billRepository,
                           ClientRepository clientRepository,
                           EmployeeRepository employeeRepository,
                           DishRepository dishRepository,
                           DrinkRepository drinkRepository,
                           StorageRepository storageRepository) {
        this.billRepository = billRepository;
        this.clientRepository = clientRepository;
        this.employeeRepository = employeeRepository;
        this.dishRepository = dishRepository;
        this.drinkRepository = drinkRepository;
        this.storageRepository = storageRepository;
    }

    @Override
    public Long createBill(Long employeeId, Long clientId) {

        Client client = clientRepository.getClientById(clientId).orElseThrow(NotFoundException::new);
        Employee employee = employeeRepository.getEmployeeById(employeeId).orElseThrow(NotFoundException::new);

        Bill bill = billRepository.createBill(BIG_DECIMAL_ZERO, BIG_DECIMAL_ZERO, employee, client, LocalDateTime.now());

        return bill.getId();
    }

    @Override
    public Long updateBill(Long id, BigDecimal summaryPrice, BigDecimal tip, Employee employee, Client client, LocalDateTime date) {

        Optional<Bill> bill = billRepository.updateBill(id, summaryPrice, tip, employee, client, date);

        return bill.get().getId();
    }

    @Override
    public Optional<Bill> getBillById(Long id) {
        return billRepository.getBillById(id);
    }

    @Override
    public List<Bill> getAllBills() {
        return billRepository.getAllBills();
    }

//    @Override
//    public List<Bill> getBillsByDate(LocalDate date) {
//        return billRepository.getBillsByDate(date);
//    }

    @Override
    public Bill addDrinkToBill(Long drinkId, Long billId) {

        Drink drink = drinkRepository.getDrinkById(drinkId).orElseThrow(NotFoundException::new);
        Bill bill = billRepository.getBillById(billId).orElseThrow(NotFoundException::new);

        BigDecimal newPrice = checkClientDiscount(bill, drink);
        BigDecimal priceWithDiscount = bill.getPriceWithDiscount().add(newPrice);

        Bill newBill = billRepository.addDrink(drink, bill);

        updateBill(
                billId,
                priceWithDiscount,
                bill.getTip(),
                bill.getEmployee(),
                bill.getClient(),
                bill.getDate());

        return newBill;
    }

    @Override
    public Bill addDishToBill(Long dishId, Long billId) {

        Dish dish = dishRepository.getDishById(dishId).orElseThrow(NotFoundException::new);
        Bill bill = billRepository.getBillById(billId).orElseThrow(NotFoundException::new);

        BigDecimal newPrice = checkClientDiscount(bill, dish);
        BigDecimal priceWithDiscount = bill.getPriceWithDiscount().add(newPrice);

        Bill newBill = billRepository.addDish(dish, bill);

        updateBill(
                billId,
                priceWithDiscount,
                bill.getTip(),
                bill.getEmployee(),
                bill.getClient(),
                bill.getDate());

        return newBill;
    }

    private BigDecimal checkClientDiscount(Bill bill, Drink drink) {

        BigDecimal clientDiscount = bill.getClient().getDiscount();
        BigDecimal newDrinkPrice = drink.getPrice();
        if (clientDiscount.compareTo(BIG_DECIMAL_ZERO) > 0){
            newDrinkPrice = newDrinkPrice.subtract(newDrinkPrice.multiply(clientDiscount));
        }
        return newDrinkPrice;
    }

    private BigDecimal checkClientDiscount(Bill bill, Dish dish) {

        BigDecimal clientDiscount = bill.getClient().getDiscount();
        BigDecimal newDishPrice = dish.getPrice();
        if (clientDiscount.compareTo(BIG_DECIMAL_ZERO) > 0){
            newDishPrice = newDishPrice.subtract(newDishPrice.multiply(clientDiscount));
        }
        return newDishPrice;
    }

    @Override
    public void removeDish(Long dishId, Long billId) {

        Dish dish = dishRepository.getDishById(dishId).orElseThrow(NotFoundException::new);
        Bill bill = billRepository.getBillById(billId).orElseThrow(NotFoundException::new);

        BigDecimal newPrice = checkClientDiscount(bill, dish);

        billRepository.removeDish(dish, bill);

        BigDecimal priceWithDiscount = bill.getPriceWithDiscount().subtract(newPrice);


        updateBill(
                billId,
                priceWithDiscount,
                bill.getTip(),
                bill.getEmployee(),
                bill.getClient(),
                bill.getDate());
    }

    @Override
    public void removeDrink(Long drinkId, Long billId) {

        Drink drink = drinkRepository.getDrinkById(drinkId).orElseThrow(NotFoundException::new);
        Bill bill = billRepository.getBillById(billId).orElseThrow(NotFoundException::new);

        BigDecimal newPrice = checkClientDiscount(bill, drink);

        billRepository.removeDrink(drink, bill);

        BigDecimal priceWithDiscount = bill.getPriceWithDiscount().subtract(newPrice);

        updateBill(
                billId,
                priceWithDiscount,
                bill.getTip(),
                bill.getEmployee(),
                bill.getClient(),
                bill.getDate());
    }

    @Override
    public void removeBill(Long id) {
        billRepository.removeBill(id);
    }
}
