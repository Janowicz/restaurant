package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.Storage;
import com.example.restaurant.domain.repository.StorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StorageServiceImpl implements StorageService{

    private final StorageRepository storageRepository;

    @Autowired
    public StorageServiceImpl(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }

    @Override
    public Long createStorage(String name, Double quantity) {

        Storage storage = storageRepository.createStorage(name, quantity);

        return storage.getId();
    }

    @Override
    public Long updateStorage(Long id, String name, Double quantity) {

        Storage storage = storageRepository.updateStorage(id, name, quantity);

        return storage.getId();
    }

    @Override
    public Optional<Storage> getStorageById(Long id) {
        return storageRepository.getStorageById(id);
    }

    @Override
    public Optional<Storage> getStorageByName(String name) {
        return storageRepository.getStorageByName(name);
    }

    @Override
    public List<Storage> getAllStorages() {
        return storageRepository.getAllStorages();
    }

    @Override
    public void removeStorage(Long id) {
        storageRepository.removeStorage(id);
    }
}
