package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.Dish;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface DishService {

    Long createDish(String name, String description, BigDecimal price, Boolean isAvailable);

    Long updateDish(Long id, String name, String description, BigDecimal price, Boolean isAvailable);

    Optional<Dish> getDishById(Long id);

    Optional<Dish> getDishByName(String name);

    List<Dish> getAllDishes();

    void removeDish(Long id);
}
