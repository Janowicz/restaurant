package com.example.restaurant.domain.service;

import com.example.restaurant.domain.model.Client;
import com.example.restaurant.domain.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Long createClient(String firstname, String lastname, BigDecimal discount) {

        Client client = clientRepository.createClient(firstname, lastname, discount);

        return client.getId();
    }

    @Override
    public Long updateClient(Long id, String firstname, String lastname, BigDecimal discount) {

        Optional<Client> client = clientRepository.updateClient(id, firstname, lastname, discount);

        return client.get().getId();
    }

    @Override
    public Optional<Client> getClient(Long id) {
        return clientRepository.getClientById(id);
    }

    @Override
    public List<Client> getAllClients() {
        return clientRepository.getAllClients();
    }

    @Override
    public void removeClient(Long id) {
        clientRepository.removeClient(id);
    }
}
