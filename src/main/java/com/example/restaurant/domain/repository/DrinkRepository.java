package com.example.restaurant.domain.repository;

import com.example.restaurant.domain.model.Dish;
import com.example.restaurant.domain.model.Drink;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface DrinkRepository {

    Drink createDrink(String name, String description, BigDecimal price, Integer portion, boolean isAvailable);

    Optional<Drink> getDrinkById(Long id);

    Optional<Drink> getDrinkByName(String name);

    List<Drink> getAllDrinks();

    Optional<Drink> updateDrink(Long id, String name, String description, BigDecimal price, Integer portion, boolean isAvailable);

    void removeDrink(Long id);
}
