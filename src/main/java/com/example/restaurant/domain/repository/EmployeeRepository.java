package com.example.restaurant.domain.repository;

import com.example.restaurant.domain.model.Employee;
import com.example.restaurant.infrastructure.entity.EPosition;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {

    Employee createEmployee(String firstname, String lastname, String pesel, EPosition position, BigDecimal salary, Employee boss);

    Employee updateEmployee(Long id, String firstname, String lastname, String pesel, EPosition position, BigDecimal salary, Employee boss);

    Optional<Employee> getEmployeeById(Long id);

    List<Employee> getAllEmployees();

    void removeEmployee(Long id);
}
