package com.example.restaurant.domain.repository;

import com.example.restaurant.domain.model.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface BillRepository {

    Bill createBill(BigDecimal summaryPrice, BigDecimal tip, Employee employee, Client client, LocalDateTime date);

    Optional<Bill> updateBill(Long id, BigDecimal summaryPrice, BigDecimal tip, Employee employee, Client client, LocalDateTime date);

    Optional<Bill> getBillById(Long id);

    Bill addDish(Dish dish, Bill bill);

    Bill addDrink(Drink drink, Bill bill);

    Bill removeDish(Dish dish, Bill bill);

    Bill removeDrink(Drink drink, Bill bill);

//    Integer changeQuantityDish(Long dishId, Long billId, Integer quantity);

//    Integer changeQuantityDrink(Long drinkId, Long billId, Integer quantity);

    List<Bill> getAllBills();

//    List<Bill> getBillsByDate(LocalDate date);

    List<Bill> getAllBillsByClientId(Long clientId);

    List<Bill> getAllBillsByEmployeeId(Long employeeId);

    void removeBill(Long id);

}
