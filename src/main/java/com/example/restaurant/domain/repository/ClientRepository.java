package com.example.restaurant.domain.repository;

import com.example.restaurant.domain.model.Client;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface ClientRepository {

    Client createClient(String firstname, String lastname, BigDecimal discount);

    Optional<Client> updateClient(Long id, String firstname, String lastname, BigDecimal discount);

    Optional<Client> getClientById(Long id);

    List<Client> getAllClients();

    List<Client> getAllClientsByFirstname(String firstname);

    List<Client> getAllClientsByLastname(String lastname);

    void removeClient(Long id);
}
