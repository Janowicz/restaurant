package com.example.restaurant.domain.repository;

import com.example.restaurant.domain.model.Dish;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface DishRepository {

    Dish createDish(String name, String description, BigDecimal price, Boolean isAvailable);

    Optional<Dish> getDishById(Long id);

    Optional<Dish> getDishByName(String name);

    List<Dish> getAllDishes();

    Optional<Dish> updateDish(Long id, String name, String description, BigDecimal price, Boolean isAvailable);

    void removeDish(Long id);

}
