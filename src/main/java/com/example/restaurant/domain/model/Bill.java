package com.example.restaurant.domain.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Bill {

    private Long id;
    private BigDecimal priceWithDiscount;
    private BigDecimal tip;
    private Employee employee;
    private Client client;
    private LocalDateTime date;
    private List<Dish> dishes;
    private List<Drink> drinks;

    public Bill() {
    }

    public Bill(Long id, BigDecimal priceWithDiscount, BigDecimal tip, Employee employee, Client client, LocalDateTime date) {
        this.id = id;
        this.priceWithDiscount = priceWithDiscount;
        this.tip = tip;
        this.employee = employee;
        this.client = client;
        this.date = date;
    }

    public Bill(Long id,
                BigDecimal priceWithDiscount,
                BigDecimal tip,
                Employee employee,
                Client client,
                LocalDateTime date,
                List<Dish> dishes,
                List<Drink> drinks) {
        this.id = id;
        this.priceWithDiscount = priceWithDiscount;
        this.tip = tip;
        this.employee = employee;
        this.client = client;
        this.date = date;
        this.dishes = dishes;
        this.drinks = drinks;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPriceWithDiscount() {
        return priceWithDiscount;
    }

    public void setPriceWithDiscount(BigDecimal priceWithDiscount) {
        this.priceWithDiscount = priceWithDiscount;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public void setTip(BigDecimal tip) {
        this.tip = tip;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public List<Dish> getDishes() {
        if (dishes == null){
            dishes = new ArrayList<>();
        }
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public List<Drink> getDrinks() {
        if (drinks == null){
            drinks = new ArrayList<>();
        }
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bill bill = (Bill) o;
        return Objects.equals(id, bill.id) &&
                Objects.equals(priceWithDiscount, bill.priceWithDiscount) &&
                Objects.equals(tip, bill.tip) &&
                Objects.equals(employee, bill.employee) &&
                Objects.equals(client, bill.client) &&
                Objects.equals(date, bill.date) &&
                Objects.equals(dishes, bill.dishes) &&
                Objects.equals(drinks, bill.drinks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, priceWithDiscount, tip, employee, client, date, dishes, drinks);
    }

    @Override
    public String toString() {
        return "Bill{" +
                "id=" + id +
                ", summaryPrice=" + priceWithDiscount +
                ", tip=" + tip +
                ", employee=" + employee +
                ", client=" + client +
                ", date=" + date +
                ", dishes=" + dishes +
                ", drinks=" + drinks +
                '}';
    }
}
