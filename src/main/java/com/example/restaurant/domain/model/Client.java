package com.example.restaurant.domain.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Client {

    private Long id;
    private String firstname;
    private String lastname;
    private BigDecimal discount;

    public Client() {
    }

    public Client(Long id, String firstname, String lastname, BigDecimal discount) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.discount = discount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(id, client.id) &&
                Objects.equals(firstname, client.firstname) &&
                Objects.equals(lastname, client.lastname) &&
                Objects.equals(discount, client.discount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstname, lastname, discount);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", discount=" + discount +
                '}';
    }
}
