package com.example.restaurant.domain.model;

import com.example.restaurant.infrastructure.entity.BillEntity;
import com.example.restaurant.infrastructure.entity.EPosition;
import com.example.restaurant.infrastructure.entity.EmployeeEntity;

import java.math.BigDecimal;
import java.util.List;

public class Employee {

    private Long id;
    private String firstname;
    private String lastname;
    private String pesel;
    private EPosition position;
    private BigDecimal salary;
    private Employee boss;
    private List<Bill> bills;

    public Employee() {
    }

    public Employee(Long id, String firstname, String lastname, String pesel, EPosition position, BigDecimal salary, Employee boss) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.pesel = pesel;
        this.position = position;
        this.salary = salary;
        this.boss = boss;
    }

    public Employee(Long id, String firstname, String lastname, String pesel, EPosition position, BigDecimal salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.pesel = pesel;
        this.position = position;
        this.salary = salary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public EPosition getPosition() {
        return position;
    }

    public void setPosition(EPosition position) {
        this.position = position;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Employee getBoss() {
        return boss;
    }

    public void setBoss(Employee boss) {
        this.boss = boss;
    }

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", position=" + position +
                ", salary=" + salary +
                ", boss=" + boss +
                ", bills=" + bills +
                '}';
    }
}
