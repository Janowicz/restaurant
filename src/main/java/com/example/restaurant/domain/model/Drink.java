package com.example.restaurant.domain.model;

import com.example.restaurant.infrastructure.entity.BillEntity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class Drink {

    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private Integer portion;
    private boolean isAvailable;
    private List<Bill> bills;

    public Drink() {
    }

    public Drink(Long id, String name, String description, BigDecimal price, Integer portion, boolean isAvailable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.portion = portion;
        this.isAvailable = isAvailable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getPortion() {
        return portion;
    }

    public void setPortion(Integer portion) {
        this.portion = portion;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Drink drink = (Drink) o;
        return isAvailable == drink.isAvailable &&
                Objects.equals(id, drink.id) &&
                Objects.equals(name, drink.name) &&
                Objects.equals(description, drink.description) &&
                Objects.equals(price, drink.price) &&
                Objects.equals(portion, drink.portion) &&
                Objects.equals(bills, drink.bills);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, portion, isAvailable, bills);
    }

    @Override
    public String toString() {
        return "Drink{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", portion=" + portion +
                ", isAvailable=" + isAvailable +
                ", bills=" + bills +
                '}';
    }
}
