package com.example.restaurant.domain.model;

import com.example.restaurant.infrastructure.entity.DishEntity;
import com.example.restaurant.infrastructure.entity.DrinkEntity;

import java.util.List;
import java.util.Objects;

public class Storage {

    private Long id;
    private String name;
    private Double quantity;
    private List<Drink> drink;
    private List<Dish> dish;

    public Storage() {
    }

    public Storage(Long id, String name, Double quantity) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public List<Drink> getDrink() {
        return drink;
    }

    public void setDrink(List<Drink> drink) {
        this.drink = drink;
    }

    public List<Dish> getDish() {
        return dish;
    }

    public void setDish(List<Dish> dish) {
        this.dish = dish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Storage storage = (Storage) o;
        return Objects.equals(id, storage.id) &&
                Objects.equals(name, storage.name) &&
                Objects.equals(quantity, storage.quantity) &&
                Objects.equals(drink, storage.drink) &&
                Objects.equals(dish, storage.dish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, quantity, drink, dish);
    }

    @Override
    public String toString() {
        return "Storage{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", drink=" + drink +
                ", dish=" + dish +
                '}';
    }
}
