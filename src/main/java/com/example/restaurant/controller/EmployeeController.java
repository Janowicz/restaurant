package com.example.restaurant.controller;

import com.example.restaurant.controller.dto.EmployeeDTO;
import com.example.restaurant.domain.model.Employee;
import com.example.restaurant.domain.service.EmployeeService;
import com.example.restaurant.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping(path = "/{id}")
    public EmployeeDTO getEmployee(@PathVariable("id") Long id){

        Optional<Employee> employeeOptional = employeeService.getEmployee(id);
        if (!employeeOptional.isPresent()){
            throw new NotFoundException();
        }

        return new EmployeeDTO(employeeOptional.get());
    }

    @GetMapping(path = "/all")
    public List<EmployeeDTO> getAllEmployees(){

        return employeeService.getAllEmployees().stream().map(EmployeeDTO::new).collect(Collectors.toList());
    }

    @PostMapping(path = "add")
    public Long createEmployee(@RequestBody EmployeeDTO employeeDTO){

        return employeeService.createEmployee(
                employeeDTO.getFirstname(),
                employeeDTO.getLastname(),
                employeeDTO.getPesel(),
                employeeDTO.getPosition(),
                employeeDTO.getSalary(),
                employeeDTO.getBoss());
    }

    @PutMapping(path = "/update/{id}")
    public Long updateEmployee(@RequestBody EmployeeDTO employeeDTO, @PathVariable("id") Long id){

        if (!employeeService.getEmployee(id).isPresent()){
            throw new NotFoundException();
        }

        return employeeService.updateEmployee(
                        id,
                employeeDTO.getFirstname(),
                employeeDTO.getLastname(),
                employeeDTO.getPesel(),
                employeeDTO.getPosition(),
                employeeDTO.getSalary(),
                employeeDTO.getBoss());
    }

    @DeleteMapping(path = "/remove/{id}")
    public void removeEmployee(@PathVariable("id") Long id){

        Optional<Employee> employeeOptional = employeeService.getEmployee(id);
        if (!employeeOptional.isPresent()){
            throw new NotFoundException();
        }

        employeeService.removeEmployee(employeeOptional.get().getId());
    }


}
