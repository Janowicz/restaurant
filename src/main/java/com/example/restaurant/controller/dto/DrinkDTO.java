package com.example.restaurant.controller.dto;

import com.example.restaurant.domain.model.Drink;

import java.io.Serializable;
import java.math.BigDecimal;

public class DrinkDTO implements Serializable {

    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private Integer portion;
    private boolean isAvailable;

    public DrinkDTO() {
    }

    public DrinkDTO(Drink drink) {
        this.id = drink.getId();
        this.name = drink.getName();
        this.description = drink.getDescription();
        this.price = drink.getPrice();
        this.portion = drink.getPortion();
        this.isAvailable = drink.isAvailable();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getPortion() {
        return portion;
    }

    public void setPortion(Integer portion) {
        this.portion = portion;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public Drink toDomain(){
        return new Drink(
                id,
                name,
                description,
                price,
                portion,
                isAvailable);
    }
}
