package com.example.restaurant.controller.dto;

import com.example.restaurant.domain.model.Client;

import java.io.Serializable;
import java.math.BigDecimal;

public class ClientDTO implements Serializable {

    private Long id;
    private String firstname;
    private String lastname;
    private BigDecimal discount;

    public ClientDTO() {
    }

    public ClientDTO(Client client) {
        this.id = client.getId();
        this.firstname = client.getFirstname();
        this.lastname = client.getLastname();
        this.discount = client.getDiscount();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Client toDomain(){
        return new Client(
                id,
                firstname,
                lastname,
                discount);
    }
}
