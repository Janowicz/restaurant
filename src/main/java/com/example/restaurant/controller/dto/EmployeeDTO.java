package com.example.restaurant.controller.dto;

import com.example.restaurant.domain.model.Employee;
import com.example.restaurant.infrastructure.entity.EPosition;

import java.io.Serializable;
import java.math.BigDecimal;

public class EmployeeDTO implements Serializable {

    private Long id;
    private String firstname;
    private String lastname;
    private String pesel;
    private EPosition position;
    private BigDecimal salary;
    private Employee boss;

    public EmployeeDTO() {
    }

    public EmployeeDTO(Employee employee) {
        this.id = employee.getId();
        this.firstname = employee.getFirstname();
        this.lastname = employee.getLastname();
        this.pesel = employee.getPesel();
        this.position = employee.getPosition();
        this.salary = employee.getSalary();
        this.boss = employee.getBoss();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public EPosition getPosition() {
        return position;
    }

    public void setPosition(EPosition position) {
        this.position = position;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Employee getBoss() {
        return boss;
    }

    public void setBoss(Employee boss) {
        this.boss = boss;
    }

    public Employee toDomain(){
        return new Employee(
                id,
                firstname,
                lastname,
                pesel,
                position,
                salary,
                boss);
    }

}
