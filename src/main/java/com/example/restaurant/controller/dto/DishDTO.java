package com.example.restaurant.controller.dto;

import com.example.restaurant.domain.model.Dish;

import java.io.Serializable;
import java.math.BigDecimal;

public class DishDTO implements Serializable {

    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private boolean isAvailable;

    public DishDTO() {
    }

    public DishDTO(Dish dish) {
        this.id = dish.getId();
        this.name = dish.getName();
        this.description = dish.getDescription();
        this.price = dish.getPrice();
        this.isAvailable = dish.isAvailable();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public Dish toDomain(){
        return new Dish(
                id,
                name,
                description,
                price,
                isAvailable);
    }
}
