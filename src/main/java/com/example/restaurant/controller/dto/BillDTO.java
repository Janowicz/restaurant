package com.example.restaurant.controller.dto;

import com.example.restaurant.domain.model.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


public class BillDTO implements Serializable {

    private Long id;
    private BigDecimal summaryPrice;
    private BigDecimal tip;
    private Employee employee;
    private Client client;
    private LocalDateTime date;
    private List<Dish> dishes;
    private List<Drink> drinks;

    public BillDTO() {
    }

    public BillDTO(Bill bill) {
        this.id = bill.getId();
        this.summaryPrice = bill.getPriceWithDiscount();
        this.tip = bill.getTip();
        this.employee = bill.getEmployee();
        this.client = bill.getClient();
        this.date = bill.getDate();
        this.dishes = bill.getDishes();
        this.drinks = bill.getDrinks();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSummaryPrice() {
        return summaryPrice;
    }

    public void setSummaryPrice(BigDecimal summaryPrice) {
        this.summaryPrice = summaryPrice;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public void setTip(BigDecimal tip) {
        this.tip = tip;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    public Bill toDomain(){
        return new Bill(
                id,
                summaryPrice,
                tip,
                employee,
                client,
                date,
                dishes,
                drinks);
    }
}
