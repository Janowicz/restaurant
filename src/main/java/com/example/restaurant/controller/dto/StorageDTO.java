package com.example.restaurant.controller.dto;

import com.example.restaurant.domain.model.Dish;
import com.example.restaurant.domain.model.Drink;
import com.example.restaurant.domain.model.Storage;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;

public class StorageDTO implements Serializable {

    private Long id;
    private String name;
    private Double quantity;
    @JsonIgnore
    private List<Drink> drink;
    @JsonIgnore
    private List<Dish> dish;

    public StorageDTO() {
    }

    public StorageDTO(Storage storage) {
        this.id = storage.getId();
        this.name = storage.getName();
        this.quantity = storage.getQuantity();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public List<Drink> getDrink() {
        return drink;
    }

    public void setDrink(List<Drink> drink) {
        this.drink = drink;
    }

    public List<Dish> getDish() {
        return dish;
    }

    public void setDish(List<Dish> dish) {
        this.dish = dish;
    }

    public Storage toDomain(){
        return new Storage(
                id,
                name,
                quantity);
    }
}
