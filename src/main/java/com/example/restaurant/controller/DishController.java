package com.example.restaurant.controller;

import com.example.restaurant.controller.dto.DishDTO;
import com.example.restaurant.domain.model.Dish;
import com.example.restaurant.domain.service.DishService;
import com.example.restaurant.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/dish")
public class DishController {

    private final DishService dishService;

    @Autowired
    public DishController(DishService dishService) {
        this.dishService = dishService;
    }

    @GetMapping(path = "/{id}")
    public DishDTO getDish(@PathVariable("id") Long id){

        Optional<Dish> dishOptional = dishService.getDishById(id);
        if (!dishOptional.isPresent()){
            throw new NotFoundException();
        }

        return new DishDTO(dishOptional.get());
    }

    @GetMapping(path = "/all")
    public List<DishDTO> getAllDishes(){

        return dishService.getAllDishes().stream().map(DishDTO::new).collect(Collectors.toList());
    }

    @PostMapping(path = "/add")
    public Long createDish(@RequestBody DishDTO dishDTO){

        return dishService.createDish(
                dishDTO.getName(),
                dishDTO.getDescription(),
                dishDTO.getPrice(),
                dishDTO.isAvailable());
    }

    @PutMapping(path = "/update/{id}")
    public Long updateDish(@RequestBody DishDTO dishDTO, @PathVariable Long id){

        Optional<Dish> optionalDish = dishService.getDishById(id);
        if (!optionalDish.isPresent()){
            throw new NotFoundException();
        }

        dishService.updateDish(
                id,
                dishDTO.getName(),
                dishDTO.getDescription(),
                dishDTO.getPrice(),
                dishDTO.isAvailable());

        return dishService.getDishById(id).get().getId();
    }

    @DeleteMapping(path = "/remove/{id}")
    public void removeDish(@PathVariable("id") Long id){

        if (!dishService.getDishById(id).isPresent()){
            throw new NotFoundException();
        }
        dishService.removeDish(id);
    }

}
