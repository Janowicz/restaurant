package com.example.restaurant.controller;

import com.example.restaurant.controller.dto.ClientDTO;
import com.example.restaurant.domain.model.Client;
import com.example.restaurant.domain.service.ClientService;
import com.example.restaurant.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/client")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping(path = "/add")
    public Long createClient(@RequestBody ClientDTO clientDTO){
        Long clientId = clientService.createClient(
                clientDTO.getFirstname(),
                clientDTO.getLastname(),
                clientDTO.getDiscount());

        return clientId;
    }

    @GetMapping(path = "/{id}")
    public ClientDTO getClientById(@PathVariable("id") Long id){

        Optional<Client> clientOptional = clientService.getClient(id);
        if (!clientOptional.isPresent()){
            throw new NotFoundException();
        }

        return new ClientDTO(clientOptional.get());
    }

    @GetMapping(path = "/all")
    public List<ClientDTO> getAllClients(){

        List<Client> clients = clientService.getAllClients();

        return clients.stream().map(ClientDTO::new).collect(Collectors.toList());
    }

    @PutMapping(path = "/update/{id}")
    public Long updateClient(@RequestBody ClientDTO clientDTO, @PathVariable("id") Long id){

        if (!clientService.getClient(id).isPresent()){
            throw new NotFoundException();
        }
        Long clientId = clientService.updateClient(
                clientDTO.getId(),
                clientDTO.getFirstname(),
                clientDTO.getLastname(),
                clientDTO.getDiscount());

        return clientId;
    }

    @DeleteMapping(path = "/remove/{id}")
    public void removeClient(@PathVariable("id") Long id){

        if (!clientService.getClient(id).isPresent()){
            throw new NotFoundException();
        }

        clientService.removeClient(id);
    }

}
