package com.example.restaurant.controller;

import com.example.restaurant.controller.dto.StorageDTO;
import com.example.restaurant.domain.model.Storage;
import com.example.restaurant.domain.service.StorageService;
import com.example.restaurant.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/storage")
public class StorageController {

    private final StorageService storageService;

    @Autowired
    public StorageController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping(path = "/add")
    public Long createStorage(StorageDTO storageDTO){

        return storageService.createStorage(
                storageDTO.getName(),
                storageDTO.getQuantity());
    }

    @GetMapping(path = "/{id}")
    public StorageDTO getStorage(@PathVariable("id") Long id){

        Optional<Storage> storageOptional = storageService.getStorageById(id);
        if (!storageOptional.isPresent()){
            throw new NotFoundException();
        }
        return new StorageDTO(storageOptional.get());
    }

    @GetMapping(path = "/all")
    public List<StorageDTO> getAllStorages(){

        return storageService.getAllStorages().stream().map(StorageDTO::new).collect(Collectors.toList());
    }

    @PutMapping(path = "/update/{id}")
    public Long updateStorage(@RequestBody StorageDTO storageDTO, @PathVariable("id") Long id){

        Optional<Storage> storageOptional = storageService.getStorageById(id);
        if (!storageOptional.isPresent()){
            throw new NotFoundException();
        }

        return storageService.updateStorage(
                id,
                storageDTO.getName(),
                storageDTO.getQuantity());
    }

    @DeleteMapping(path = "/remove/{id}")
    public void removeStorage(@PathVariable("id") Long id){
        if (!storageService.getStorageById(id).isPresent()){
            throw new NotFoundException();
        }

        storageService.removeStorage(id);
    }

}
