package com.example.restaurant.controller;

import com.example.restaurant.controller.dto.BillDTO;
import com.example.restaurant.domain.model.Bill;
import com.example.restaurant.domain.service.*;
import com.example.restaurant.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/bill")
public class BillController {

    private final BillService billService;
    private final ClientService clientService;
    private final EmployeeService employeeService;
    private final DishService dishService;
    private final DrinkService drinkService;

    @Autowired
    public BillController(BillService billService,
                          ClientService clientService,
                          EmployeeService employeeService,
                          DishService dishService,
                          DrinkService drinkService) {
        this.billService = billService;
        this.clientService = clientService;
        this.employeeService = employeeService;
        this.dishService = dishService;
        this.drinkService = drinkService;
    }

    @GetMapping(path = "/{id}")
    public BillDTO getBillById(@PathVariable("id") Long id){

        Optional<Bill> billOptional = billService.getBillById(id);
        if (!billOptional.isPresent()){
            throw new NotFoundException();
        }

        return new BillDTO(billOptional.get());
    }

    @GetMapping(path = "/all")
    public List<BillDTO> getAllBills(){

        return billService.getAllBills().stream().map(BillDTO::new).collect(Collectors.toList());
    }

    @PostMapping(path = "/create")
    public Long createBill(
            @RequestParam(name = "employeeId") Long employeeId,
            @RequestParam(name = "clientId") Long clientId){

        return billService.createBill(employeeId, clientId);
    }

    @DeleteMapping(path = "/removeBill/{id}")
    public void removeBill(@PathVariable("id") Long id){

        billService.removeBill(id);
    }

    @PutMapping(path = "/addDrink")
    public void addDrinkToBill(
            @RequestParam(name = "billId") Long billId,
            @RequestParam(name = "drinkId") Long drinkId){

        billService.addDrinkToBill(drinkId, billId);
    }

    @PutMapping(path = "/addDish")
    public void addDishToBill(
            @RequestParam(name = "billId") Long billId,
            @RequestParam(name = "dishId") Long dishId){

        billService.addDishToBill(dishId, billId);
    }

    @DeleteMapping(path = "/removeDrink")
    public void removeDrinkFromBill(
            @RequestParam(name = "billId") Long billId,
            @RequestParam(name = "drinkId") Long drinkId){

        billService.removeDrink(drinkId, billId);
    }

    @DeleteMapping(path = "/removeDish")
    public void removeDishFromBill(
            @RequestParam(name = "billId") Long billId,
            @RequestParam(name = "dishId") Long dishId){

        billService.removeDish(dishId, billId);
    }

}
