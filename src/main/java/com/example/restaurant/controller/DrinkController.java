package com.example.restaurant.controller;

import com.example.restaurant.controller.dto.DrinkDTO;
import com.example.restaurant.domain.model.Drink;
import com.example.restaurant.domain.service.DrinkService;
import com.example.restaurant.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/drink")
public class DrinkController {

    private final DrinkService drinkService;

    @Autowired
    public DrinkController(DrinkService drinkService) {
        this.drinkService = drinkService;
    }

    @GetMapping(path = "/{id}")
    public DrinkDTO getDrink(@PathVariable("id") Long id){

        Optional<Drink> drinkOptional = drinkService.getDrinkById(id);
        if (!drinkOptional.isPresent()){
            throw new NotFoundException();
        }

        return new DrinkDTO(drinkOptional.get());
    }

    @GetMapping(path = "/all")
    public List<DrinkDTO> getAllDrinks(){

        return drinkService.getAllDrinks().stream().map(DrinkDTO::new).collect(Collectors.toList());
    }

    @PostMapping(path = "/add")
    public Long createDrink(@RequestBody DrinkDTO drinkDTO){

        return drinkService.createDrink(
                drinkDTO.getName(),
                drinkDTO.getDescription(),
                drinkDTO.getPrice(),
                drinkDTO.getPortion(),
                drinkDTO.isAvailable());
    }

    @PutMapping(path = "/update/{id}")
    public Long updateDrink(@RequestBody DrinkDTO drinkDTO, @PathVariable Long id){

        Optional<Drink> optionalDrink = drinkService.getDrinkById(id);
        if (!optionalDrink.isPresent()){
            throw new NotFoundException();
        }

        drinkService.updateDrink(
                id,
                drinkDTO.getName(),
                drinkDTO.getDescription(),
                drinkDTO.getPrice(),
                drinkDTO.getPortion(),
                drinkDTO.isAvailable());

        return drinkService.getDrinkById(id).get().getId();
    }

    @DeleteMapping(path = "/remove/{id}")
    public void removeDrink(@PathVariable("id") Long id){

        if (!drinkService.getDrinkById(id).isPresent()){
            throw new NotFoundException();
        }
        drinkService.removeDrink(id);
    }
}
