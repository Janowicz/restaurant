**Technologies:** REST, Spring Boot, Spring Data, Hibernate, PostreSQL , Maven, Git, GitLab.

**Design patterns:** Adapter, DAO

Restaurant app with technologies: REST, Spring Boot, Spring Data, Hibernate, Maven, Git, GitLab, PostreSQL. Application use CRUD methods with connection with database (PostreSQL).


**Endpoints:

Drink:**

GET http://localhost:8282/drink/all

GET http://localhost:8282/drink/{id}

POST http://localhost:8282/drink/add JSON body

PUT http://localhost:8282/drink/update/{id} JSON body

DELETE http://localhost:8282/drink/remove/{id}

```
JSON example:
{
    "id": 401,
    "name": "Small water",
    "description": "Water in a bottle 0.5l",
    "price": 2.99,
    "portion": 500,
    "isAvailable": true
}
```


**Dish:**

GET http://localhost:8282/dish/all

GET http://localhost:8282/dish/{id}

POST http://localhost:8282/dish/add JSON body

PUT http://localhost:8282/dish/update/{id} JSON body

DELETE http://localhost:8282/dish/remove/{id}

```
JSON example:
{
    "id": 301,
    "name": "Small kebab",
    "description": "Small kebab with chicken",
    "price": 12.99,
    "isAvailable": true
}
```


**Client:**

GET http://localhost:8282/client/all

GET http://localhost:8282/client/{id}

POST http://localhost:8282/client/add JSON body

PUT http://localhost:8282/client/update/{id} JSON body

DELETE http://localhost:8282/client/remove/{id}

```
JSON example:
{
    "id": 201,
    "firstname": "Jan",
    "lastname": "Kowalski",
    "discount": 0.10
}
```


**Bill:**

GET http://localhost:8282/bill/all

GET http://localhost:8282/bill/{id}

POST http://localhost:8282/bill/create Params: employeeId, clientId

PUT http://localhost:8282/bill/addDrink Params: billId, drinkId

PUT http://localhost:8282/bill/addDish Params: billId, dishId

DELETE http://localhost:8282/client/removeBill/{id}

DELETE http://localhost:8282/bill/removeDrink Params: billId, drinkId

DELETE http://localhost:8282/bill/removeDish Params: billId, dishId


```
JSON example:
{
    "id": 101,
    "summaryPrice": 24.37,
    "tip": 5.00,
    "employee": {
        "id": 501,
        "firstname": "Janusz",
        "lastname": "Januszewski",
        "pesel": "12345",
        "position": "RESTAURANT_MANAGER",
        "salary": 5500.00,
        "boss": null,
        "bills": null
    },
    "client": {
        "id": 201,
        "firstname": "Jan",
        "lastname": "Kowalski",
        "discount": 0.10
    },
    "date": "2020-05-05T12:12:12",
    "dishes": [
        {
            "id": 301,
            "name": "Small kebab",
            "description": "Small kebab with chicken",
            "price": 12.99,
            "bills": [],
            "available": true
        }
    ],
    "drinks": [
        {
            "id": 401,
            "name": "Small water",
            "description": "Water in a bottle 0.5l",
            "price": 2.99,
            "portion": 500,
            "bills": null,
            "available": true
        }
    ]
}
```


**Employee:**

GET http://localhost:8282/employee/all

GET http://localhost:8282/employee/{id}

POST http://localhost:8282/employee/add JSON body

PUT http://localhost:8282/employee/update/{id} JSON body

DELETE http://localhost:8282/employee/remove/{id}

```
JSON example:
{
    "id": 503,
    "firstname": "Karolina",
    "lastname": "Karolinowska",
    "pesel": "123457",
    "position": "BARMAN",
    "salary": 3500.00,
    "boss": {
        "id": 501,
        "firstname": "Janusz",
        "lastname": "Januszewski",
        "pesel": "12345",
        "position": "RESTAURANT_MANAGER",
        "salary": 5500.00,
        "boss": null,
        "bills": null
    }
}
```

**Storage:**

GET http://localhost:8282/storage/all

GET http://localhost:8282/storage/{id}

POST http://localhost:8282/storage/add JSON body

PUT http://localhost:8282/storage/update/{id} JSON body

DELETE http://localhost:8282/storage/remove/{id}

```
{
    "id": 602,
    "name": "Medium kebab",
    "quantity": 5.0
}
```

